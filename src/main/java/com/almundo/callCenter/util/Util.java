/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Clase con metodos utiles en todo el proyecto
 * @author Marco
 */
public class Util {
    
    public static Date addSecondsDate(Date date,int seconds){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }
    
    public static int generateNumRandom(int maxNumber,int sumNum){
        Random rand = new Random();
        int seconds = rand.nextInt(maxNumber) + sumNum; 
        return seconds;
    }
    
}
