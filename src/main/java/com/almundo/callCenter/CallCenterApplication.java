package com.almundo.callCenter;

import com.almundo.callCenter.domain.Customer;
import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.Call;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import com.almundo.callCenter.thread.Dispacher;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.almundo.callCenter.service.ICustomerService;
import com.almundo.callCenter.service.IEmployeeService;
import com.almundo.callCenter.service.ICallService;
import com.almundo.callCenter.util.Util;

@SpringBootApplication
public class CallCenterApplication implements CommandLineRunner {
    
        @Autowired
        private ICallService callService;
        @Autowired
        private ICustomerService customerService;
        @Autowired
        private IEmployeeService employeeService;
       

	public static void main(String[] args) {
            SpringApplication.run(CallCenterApplication.class, args);
	}
        
        @Override
        public void run(String... strings) throws Exception {
           createCalls();
        }        
        
        /**
         * Metodo que se encarga de ejecutar los hilos
         * @throws InterruptedException 
         */
        private void createCalls() throws InterruptedException{            
            createCurstomers();
            createEmployees();            
            for(int i=1;i<13;i++){
               Dispacher dispacher = new Dispacher(i,new Call(this.customerService.findCustomerById(new Long(Util.generateNumRandom(4,1))),null,new Date(),null),this.employeeService,this.callService);
               new Thread(dispacher).start();
            }
        }        
        
        /**
         * Metodo que se encarga de crear los clientes
         */
        private void createCurstomers(){
            Customer customer = new Customer();             
            customer.setNames("Marco");
            customer.setSurnames("Ruiz");
            customer.setIdentification(1232);
            this.customerService.saveCustomer(customer);
            
            customer = new Customer();             
            customer.setNames("Maria");
            customer.setSurnames("Torres");
            customer.setIdentification(1232);
            this.customerService.saveCustomer(customer);
            
            customer = new Customer();             
            customer.setNames("Jhon");
            customer.setSurnames("Torres");
            customer.setIdentification(1444232);
            this.customerService.saveCustomer(customer);
            
            customer = new Customer();             
            customer.setNames("Pedro");
            customer.setSurnames("Torres");
            customer.setIdentification(1444232);
            this.customerService.saveCustomer(customer);
            
        }
        
        /**
         * Metodo que se encarga de crear los empleados
         */
        private void createEmployees(){
            Employee employee = new Employee();            
            employee.setNames("Tatiana");
            employee.setSurnames("Madrid");
            employee.setIdentification(131321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Andres");
            employee.setSurnames("Lopez");
            employee.setIdentification(1412);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Camilo");
            employee.setSurnames("Atencia");
            employee.setIdentification(773737);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Julio");
            employee.setSurnames("Cardona");
            employee.setIdentification(1421212);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            this.employeeService.saveEmployee(employee);
            
            
            employee = new Employee();            
            employee.setNames("Carlos");
            employee.setSurnames("Pastrana");
            employee.setIdentification(1319191321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.SUPERVISOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Santiago");
            employee.setSurnames("Perez");
            employee.setIdentification(1317721);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.SUPERVISOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Santiago");
            employee.setSurnames("Pastrana");
            employee.setIdentification(6453517);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.SUPERVISOR);
            this.employeeService.saveEmployee(employee);
            
            
            employee = new Employee();            
            employee.setNames("Andrea");
            employee.setSurnames("Florez");
            employee.setIdentification(444353);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.DIRECTOR);
            this.employeeService.saveEmployee(employee);
            
            employee = new Employee();            
            employee.setNames("Karen");
            employee.setSurnames("Marin");
            employee.setIdentification(1277127);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.DIRECTOR);
            this.employeeService.saveEmployee(employee);
        }
        
        
        
}
