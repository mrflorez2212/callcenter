/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.domain.enumerado;

/**
 *
 * @author Marco
 */
public enum EmployeeType {
    OPERADOR,
    SUPERVISOR,
    DIRECTOR;
}
