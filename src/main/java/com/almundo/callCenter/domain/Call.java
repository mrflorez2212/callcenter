/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import lombok.Data;

/**
 * Clase para el manejo de la informacion de la llamada
 * @author Marco
 */
@Data
@Entity
public class Call  {
    @Id
    @GeneratedValue
    private Long idCall; 
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Employee employee;
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateStartCall;
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateFinishedCall;

    public Call() {
    }
    
    

    public Call(Long idCall, Customer customer, Employee employee, Date dateStartCall, Date dateFinishedCall) {
        this.idCall = idCall;
        this.customer = customer;
        this.employee = employee;
        this.dateStartCall = dateStartCall;
        this.dateFinishedCall = dateFinishedCall;
    }
    
     public Call( Customer customer, Employee employee, Date dateStartCall, Date dateFinishedCall) {
        
        this.customer = customer;
        this.employee = employee;
        this.dateStartCall = dateStartCall;
        this.dateFinishedCall = dateFinishedCall;
    }
    
    
    
}
