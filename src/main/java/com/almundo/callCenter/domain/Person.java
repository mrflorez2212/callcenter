/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Data;

/**
 *
 * @author Marco
 */
@Data
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Person {    
    @Id
    @GeneratedValue
    private Long id;
   
    @Column
    private int identification;
    @Column
    private String names;
    @Column
    private String surnames;
    
}
