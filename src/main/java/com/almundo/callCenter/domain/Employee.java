/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.domain;


import com.almundo.callCenter.domain.enumerado.EmployeeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Data;

/**
 * Clase para el manejo de los tipos de empleados
 * @author Marco
 */
@Data
@Entity
public class Employee extends Person{    
 
    @Column
    private EmployeeType employeeType;
    @Column
    private boolean available;

    public Employee() {
    }

    public Employee(Long id, int identification, String names, String surnames, EmployeeType employeeType) {        
        super.setIdentification(identification);
        super.setNames(names);
        super.setSurnames(surnames);        
        this.employeeType = employeeType;
    }
    
    
    
}
