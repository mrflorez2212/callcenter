/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.thread;

import com.almundo.callCenter.domain.Call;
import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import com.almundo.callCenter.service.ICallService;
import com.almundo.callCenter.service.IEmployeeService;
import com.almundo.callCenter.util.Util;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * clase encargada de el manejo de las llamadas
 * @author Marco
 */
public class Dispacher implements Runnable   {
    
    private static final Logger logger = Logger.getLogger("Dispacher.class");     
    private IEmployeeService employeeService;    
    private ICallService callService;    
    private Call call;
    private Employee employee;    
    private int numberCall;
    //numero de llamadas que atiende concurrentemente
    private static final Semaphore AVAILABLE=new Semaphore(10);    
    
    public Dispacher(int numberCall,Call call,IEmployeeService employeeService,ICallService callService) throws InterruptedException{        
        this.numberCall=numberCall;
        this.call = call;
        this.employeeService=employeeService;
        this.callService=callService;        
        Thread.sleep(1000);
    }
    
    @Override
    public void run() {        
        dispatchCall(Util.generateNumRandom(5,5));
    }
    
    private void waitSeconds(int seconds) {
	try {
            Thread.sleep(seconds * 1000);
	} catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
	}
    }
    
    /**
     * Metodo encargado de asignar los empleados a las llamadas.     * 
     * @param seconds 
     * @return  
     */    
    public Call dispatchCall(int seconds){
        try{ 
            long TStart, TFinish;
            logger.log(Level.INFO, "LLamada #" + this.numberCall + " Para el Cliente: "  + this.call.getCustomer().getNames()); 
            /**
             * Se coloca un semaforo, para verificar la diponibilidad
             * Con este se soluciona cuando son mas de 10 llamadas
             */            
            AVAILABLE.acquire();
            /**
             * Primero se valida que hallan operadores disponibles para atender la llamada,
             * luego supervisores y por ultimo directores
             */            
            List<Employee> listEmployee =this.employeeService.findByAvailable(EmployeeType.OPERADOR);
            if(listEmployee.isEmpty()){                
                listEmployee =this.employeeService.findByAvailable(EmployeeType.SUPERVISOR);
                if(listEmployee.isEmpty()){                    
                    listEmployee =this.employeeService.findByAvailable(EmployeeType.DIRECTOR);
                }
            }   
            
            /**
             * Se selecciona el primer empleado disponible, y se pone en no disponible
             */            
            TStart = System.currentTimeMillis();
            this.employee = listEmployee.get(0);
            this.employee.setAvailable(false);
            this.employeeService.saveEmployee(this.employee);  
            
            logger.log(Level.INFO, "La LLamada #" + this.numberCall + " Esta haciendo atendida por el " + this.employee.getEmployeeType() + " de nombre " + this.employee.getNames()  + "  Para el Cliente: "  + this.call.getCustomer().getNames());            
            waitSeconds(seconds);            
            
            /**
             * Despues de que se acaba la llamada, se vuelve a colocar el empleado disponible para recibir mas llamadas.
             * Se guarda la informacion de la llamada.
             */
            this.employee.setAvailable(true);
            this.employeeService.saveEmployee(this.employee);            
            this.call.setEmployee(this.employee);            
            this.call.setDateFinishedCall(Util.addSecondsDate(this.call.getDateStartCall(),seconds));            
            this.callService.saveCall(this.call);
            TFinish = System.currentTimeMillis();            
            logger.log(Level.INFO, "La LLamada # " + this.numberCall + " ha finalizado exitosamente por " + this.employee.getNames()  + "  Para el Cliente: "  + this.call.getCustomer().getNames()+ " con una duración de: " +   (TFinish - TStart)/1000 + " segundos"); 
            
            // Libera disponibilidad.
            AVAILABLE.release();
            
            
        }catch(Exception e){
            this.call=null;
            e.printStackTrace();
        }
        return call;
    }
 
}
