/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.repository;

import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Marco
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long>{

    @Query("SELECT e FROM Employee e WHERE e.employeeType=:employeeType and e.available=true")
    public Iterable<Employee> findByAvailable(@Param("employeeType") EmployeeType employeeType);
        
    
    
    
    
}
