/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.repository;

import com.almundo.callCenter.domain.Call;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Marco
 */
public interface CallRepository extends CrudRepository<Call, Long>{
    
}
