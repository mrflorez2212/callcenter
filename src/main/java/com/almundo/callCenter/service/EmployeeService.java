/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.service;

import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.almundo.callCenter.repository.EmployeeRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marco
 */
@Service
public class EmployeeService implements IEmployeeService{
    
    @Autowired
    private EmployeeRepository empleadoRepository;
    
    @Override    
    public void saveEmployee(Employee employee){
        empleadoRepository.save(employee);
    }
    
    @Override
    public Employee findEmployeeById(Long id){
        return empleadoRepository.findById(id).get();
    }
    
    @Override
    public List<Employee> findByAvailable(EmployeeType employeeType){
        List<Employee> listEmployee = new ArrayList<>();
        for(Employee employee:empleadoRepository.findByAvailable(employeeType)){
            listEmployee.add(employee);
        }
        return listEmployee;
    }
    
}
