/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.service;

import com.almundo.callCenter.domain.Call;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.almundo.callCenter.repository.CallRepository;

/**
 *
 * @author Marco
 */
@Service
public class CallService implements ICallService{
    
    @Autowired
    private CallRepository llamadaRepository;
    
    @Override
    public void saveCall(Call call){
        llamadaRepository.save(call);
    }
    
}
