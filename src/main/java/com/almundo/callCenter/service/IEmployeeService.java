/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.service;

import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import java.util.List;

/**
 *
 * @author Marco
 */
public interface IEmployeeService {
    public void saveEmployee(Employee employee);
    public Employee findEmployeeById(Long id);
    
    public List<Employee> findByAvailable(EmployeeType employeeType);
    
}
