/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.service;

import com.almundo.callCenter.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.almundo.callCenter.repository.CustomerRepository;

/**
 *
 * @author Marco
 */
@Service
public class CustomerService implements ICustomerService{
    
    @Autowired
    private CustomerRepository clienteRepository;
    
    @Override
    public void saveCustomer(Customer customer){
        clienteRepository.save(customer);
    }
    
    public Customer findCustomerById(Long id){
        return clienteRepository.findById(id).get();
    }
    
}
