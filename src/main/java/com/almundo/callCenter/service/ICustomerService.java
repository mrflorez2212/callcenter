/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almundo.callCenter.service;

import com.almundo.callCenter.domain.Customer;

/**
 *
 * @author Marco
 */
public interface ICustomerService {
    public void saveCustomer(Customer customer);    
    public Customer findCustomerById(Long id);
}
