package com.almundo.callCenter;

import com.almundo.callCenter.domain.Call;
import com.almundo.callCenter.domain.Customer;
import com.almundo.callCenter.domain.Employee;
import com.almundo.callCenter.domain.enumerado.EmployeeType;
import com.almundo.callCenter.service.CallService;
import com.almundo.callCenter.service.CustomerService;
import com.almundo.callCenter.service.EmployeeService;
import com.almundo.callCenter.service.ICallService;
import com.almundo.callCenter.service.ICustomerService;
import com.almundo.callCenter.service.IEmployeeService;
import com.almundo.callCenter.thread.Dispacher;
import com.almundo.callCenter.util.Util;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CallCenterApplicationTests {
    
        private ICallService callService;
        private ICustomerService customerService;
        private IEmployeeService employeeService;
        private Dispacher dispacher;
        private List<Employee> listEmployeeOperative;
        private List<Employee> listEmployeeSupervisors;
        private List<Employee> listEmployeeDirector;
    
    
        @Before
        public void configuraTestCase() throws InterruptedException {   
            
            callService =mock(CallService.class);
            customerService =mock(CustomerService.class);
            employeeService =mock(EmployeeService.class); 
            Customer  customer = new Customer();             
            customer.setNames("Pedro");
            customer.setSurnames("Torres");
            customer.setIdentification(1444232);
            dispacher = new Dispacher(1,new Call(customer,null,new Date(),null),this.employeeService,this.callService);
            createEmployees();
            
        }
    
        /**
         * Test cuando hay operararios disponibles
         */
	@Test
	public void testCreateCallOperative() {
            
            Mockito.when(employeeService.findByAvailable(EmployeeType.OPERADOR)).thenReturn(listEmployeeOperative);
            Mockito.when(employeeService.findByAvailable(EmployeeType.SUPERVISOR)).thenReturn( new ArrayList<>());
            Mockito.when(employeeService.findByAvailable(EmployeeType.DIRECTOR)).thenReturn( new ArrayList<>());
            Call call=null;
            try{
                call = dispacher.dispatchCall(Util.generateNumRandom(5,5));
            }catch(Exception e){
                call=null;
            }            
            Assert.assertNotNull(call);
	}
        
        /**
         * Test cuando no hay operario disponible, pero si hay supervisor disponible
         */
        @Test
	public void testCreateCallSupervisor() {
            
            Mockito.when(employeeService.findByAvailable(EmployeeType.OPERADOR)).thenReturn( new ArrayList<>());
            Mockito.when(employeeService.findByAvailable(EmployeeType.SUPERVISOR)).thenReturn(listEmployeeSupervisors);
            Mockito.when(employeeService.findByAvailable(EmployeeType.DIRECTOR)).thenReturn( new ArrayList<>());
            Call call=null;
            try{
                call = dispacher.dispatchCall(Util.generateNumRandom(5,5));
            }catch(Exception e){
                call=null;
            }            
            Assert.assertNotNull(call);
	}
        
        /**
         * Test cuando solo hay director disponible
         */
        @Test
	public void testCreateCallDirector() {
            
            Mockito.when(employeeService.findByAvailable(EmployeeType.OPERADOR)).thenReturn( new ArrayList<>());
            Mockito.when(employeeService.findByAvailable(EmployeeType.SUPERVISOR)).thenReturn( new ArrayList<>());
            Mockito.when(employeeService.findByAvailable(EmployeeType.DIRECTOR)).thenReturn(listEmployeeDirector);
            Call call=null;
            try{
                call = dispacher.dispatchCall(Util.generateNumRandom(5,5));
            }catch(Exception e){
                call=null;
            }            
            Assert.assertNotNull(call);
	}
        
        
        private void createEmployees(){
            listEmployeeOperative= new ArrayList<>();
            listEmployeeSupervisors= new ArrayList<>();
            listEmployeeDirector= new ArrayList<>();
            Employee employee = new Employee();            
            employee.setNames("Tatiana");
            employee.setSurnames("Madrid");
            employee.setIdentification(131321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            listEmployeeOperative.add(employee);
            
            employee = new Employee();            
            employee.setNames("Andres");
            employee.setSurnames("Lopez");
            employee.setIdentification(1412);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.OPERADOR);
            listEmployeeOperative.add(employee);
            
            employee = new Employee();            
            employee.setNames("Carlos");
            employee.setSurnames("Pastrana");
            employee.setIdentification(1319191321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.SUPERVISOR);
            listEmployeeSupervisors.add(employee);
            
            employee = new Employee();            
            employee.setNames("Santiago");
            employee.setSurnames("Pastrana");
            employee.setIdentification(1319191321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.SUPERVISOR);
            listEmployeeSupervisors.add(employee);
            
            employee = new Employee();            
            employee.setNames("Andrea");
            employee.setSurnames("Pastrana");
            employee.setIdentification(1319191321);
            employee.setAvailable(true);
            employee.setEmployeeType(EmployeeType.DIRECTOR);
            listEmployeeDirector.add(employee);            
            
        }
        
        
        

}
